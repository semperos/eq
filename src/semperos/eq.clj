(ns semperos.eq
  (:require [clojure.tools.reader.edn :as edn]
            [puget.printer :as puget])
  (:gen-class))

(defn -main
  [option & args]
  (puget/cprint
   (edn/read-string
    (case option
      "-i" (slurp *in*)
      "-e" (first args)
      (first args)))))
