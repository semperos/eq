(defproject semperos/eq "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/tools.reader "1.3.2"]
                 [mvxcvi/puget "1.2.0"]]
  :main ^:skip-aot semperos.eq
  :target-path "target/%s"
  :profiles {:dev {:dependencies [[org.clojure/clojure "1.9.0"]]}
             :native {:dependencies [[org.clojure/clojure "1.9.0"]]
                      :aot [semperos.eq]
                      :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}
             :uberjar {:aot :all}})
